<?php


/**
 * Syncronize content form.
 *
 * @param $form_state
 *   The state of the form.
 * @return
 *   The form array.
 */
function skeleton_sync_content_form($form_state) {
  drupal_add_js(drupal_get_path('module', 'skeleton') . '/skeleton-admin.js');
  $form = array();
  $form['help'] = array(
    '#prefix' => '<p>',
    '#suffix' => '</p>',
    '#value' => t('Use this form to syncronize changed templates with pages which have all ready been created.'),
  );

  // Build the list of pages which have had content changes.
  $updated = array();
  $result = db_query("SELECT stn.nid, stn.template_id, st.template FROM {skeleton_template_node} stn INNER JOIN {skeleton_template} st ON stn.template_id = st.template_id WHERE template_status = '%s'", 'updated');
  while ($eligble_node = db_fetch_object($result)) {
    $node = node_load($eligble_node->nid);
    $book = node_load($node->book['bid']);
    $updated[$node->nid] = t('<a href="@node-url">%title</a>, in the <a href="@book-url">%book-title</a> book, created from the <a href="@template-url">%template</a> template.', array('@node-url' => url('node/' . $node->nid), '%title' => $node->title, '@book-url' => url('node/' . $book->nid), '%book-title' => $book->title, '%template' => $eligble_node->template, '@template-url' => url('admin/content/skeleton/template/' . $eligble_node->template_id . '/view')));
  }
  if (!empty($updated)) {
    $form['#collapsed'] = FALSE;
    $form['nodes'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Select the nodes you wish to update'),
      '#options' => $updated,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Synchronize'),
    );
  }
  else {
    $form['no_nodes'] = array(
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#value' => t('There are no nodes eligible for content synchronizing.')
    );
  }
  return $form;
}

/**
 * Submit handler for the syncronize content form.
 *
 * @param $form
 *   The form being submitted.
 *
 * @param $form_state
 *   The state of the form.
 */
function skeleton_sync_content_form_submit($form, $form_state) {
  $batch = array(
    'title' => t('Updating content'),
    'init_message' => t('Content updating is starting.'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('There was an error while updating content.'),
    'file' => drupal_get_path('module', 'skeleton'). '/skeleton_sync.inc',
  );
  foreach($form_state['values']['nodes'] as $nid) {
    $batch['operations'][] = array('skeleton_sync_update_node', array($nid));
  }
  batch_set($batch);
}

/**
 * Form for pushing out new templates to existing skeletons.
 */
function skeleton_sync_add_template_form($form_state) {
  drupal_add_js(drupal_get_path('module', 'skeleton') . '/skeleton-admin.js');
  $form = array();
  $form['help'] = array(
    '#prefix' => '<p>',
    '#value' => 'Use this form to add new templates to existing books created from a skeleton.',
    '#suffix' => '</p>',
  );
  $form['skeletons'] = array(
    '#tree' => TRUE,
  );
  // Find all copies of instantiated skeletons which do not contain a template
  // now associated with the same skeleton.
  $skeleton_result = db_query("SELECT DISTINCT skeleton_id FROM {skeleton_template_node}");
  while ($skeleton_id = db_result($skeleton_result)) {
    $templates = array();
    $updated = array();
    $skeleton = skeleton_load($skeleton_id);
    $template_result = db_query("SELECT DISTINCT template_id FROM {skeleton_template_node} WHERE skeleton_id = %d", $skeleton_id);
    while ($template_id = db_result($template_result)) {
      $templates[] = $template_id;
    }
    $updated_result = db_query("SELECT template_id FROM {skeleton_data} WHERE skeleton_id = %d AND template_id NOT IN (" . implode(',', $templates) . ")", $skeleton_id);
    while ($updated_id = db_result($updated_result)) {
      $template = skeleton_template_load($updated_id);
      $updated[$updated_id] = l($template->template, 'admin/content/skeleton/template/' . $template->template_id . '/view');
    }
    if (!empty($updated)) {
      $form['skeletons'][$skeleton_id] = array(
        '#type' => 'checkboxes',
        '#title' => t('Create new pages from new templates assigned to <a href="@skeleton-url">%skeleton</a>', array('@skeleton-url' => url('admin/content/skeleton/skeleton/' . $skeleton->skeleton_id . '/edit'), '%skeleton' => $skeleton->skeleton)),
        '#options' => $updated,
      );
      $sync_available = TRUE;
    }
  }
  if ($sync_available) {
    $form['submit'] = array(
      '#type' => 'submit', 
      '#value' => t('Syncronize'),
    );
  }
  else {
    $form['no_templates'] = array(
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#value' => t('There are no new templates available.')
    );
  }
  return $form;
}

/**
 * Submit handler for pushing out new templates to existing books.
 */
function skeleton_sync_add_template_form_submit($form, &$form_state) {
  module_load_include('inc', 'node', 'node.pages');
  module_load_include('inc', 'skeleton', 'skeleton_token');
  foreach($form_state['values']['skeletons'] as $skeleton_id => $templates) {
    $templates = array_filter($templates);
    foreach($templates as $template_id) {
      // Find the template parent of this template
      // $template = skeleton_template_load($template_id);
      // If the parent is 0, we place it under the top-most book node.
      // Otherwise, place it under the parent template in the book.
      $parent = db_result(db_query("SELECT parent FROM {skeleton_data} WHERE skeleton_id = %d AND template_id = %d", $skeleton_id, $template_id));
      if ($parent == 0) {
        // TODO!
      }
      else {
        $parent_nids_result = db_query("SELECT nid, tokens FROM {skeleton_template_node} WHERE skeleton_id = %d AND template_id = %d", $skeleton_id, $parent);
        while($parent_info = db_fetch_object($parent_nids_result)) {
          $parent_info->tokens = unserialize($parent_info->tokens);
          $data = db_result(db_query("SELECT node_data FROM {skeleton_template} WHERE template_id = %d", $template_id));
          $node = array();
          $node['values'] = unserialize($data);
          $parent_node = node_load($parent_info->nid, NULL, TRUE);
          $node['values']['book'] = array();
          $node['values']['book']['bid'] = $parent_node->book['bid'];
          $node['values']['book']['plid'] = $parent_node->book['mlid'];
          $node['values']['book']['options'] = array();

          // Allow use of the [bookpathalias] token from pathauto.
          if (module_exists('pathauto')) {
            $node['values']['title'] = str_replace('[bookpathalias]', drupal_get_path_alias($parent_node->path), $node['values']['title']);
            $node['values']['body'] = str_replace('[bookpathalias]', drupal_get_path_alias($parent_node->path), $node['values']['body']);
          }
    
          // Replace custom tokens in the title and body fields. Replace skeleton
          // tokens first so they may be used as node tokens such as [title].
          if (!empty($parent_info->tokens)) {
            $node = _skeleton_token_replace_values($node, 'skeleton', $parent_info->tokens);
          }

          $node = _skeleton_token_replace_values($node, 'node', $node);
    
          $node['values']['uid'] = $parent_node->uid;
          $node['values']['name'] = $parent_node->name;
    
          $node['values']['tokens'] = $parent_info->tokens;
   
          // We have to add the skeleton ID here as we can't save it with the
          // template as a template may have multiple skeletons.
          $node['values']['skeleton_id'] = $skeleton_id;
    
          $node['values']['op'] = t('Save');
          drupal_execute($node['values']['type'] .'_node_form', $node, (object)$node['values']);
          // TODO: This will probably need to be removed if this call is integrated
          // into the book module. See http://drupal.org/node/364529 for details.
          menu_rebuild();
          $form_state['nids'][] = $node['nid'];
        }
      }
    }
  }
}

/**
 * Form for deleting instantiated templates where the source template has been
 * deleted.
 */
function skeleton_sync_delete_template_form($form_state) {
  drupal_add_js(drupal_get_path('module', 'skeleton') . '/skeleton-admin.js');
  $form = array();
  $form['help'] = array(
    '#prefix' => '<p>',
    '#suffix' => '</p>',
    '#value' => t("Use this form to delete nodes where the associated template has also been deleted."),
  );
  $form['nodes'] = array(
    '#tree' => TRUE,
  );

  // Find all nodes where the template it was created from has been deleted.
  $result = db_query("SELECT nid from {skeleton_template_node} stn LEFT JOIN {skeleton_template} st ON stn.template_id = st.template_id WHERE st.template_id IS NULL");
  $options = array();
  while ($node = node_load(db_result($result))) {
    $book = node_load($node->book['bid']);
    $options[$node->nid] = t('<a href="@node-url">%title</a>, in the <a href="@book-url">%book-title</a> book.', array('@node-url' => url('node/' . $node->nid), '%title' => $node->title, '@book-url' => url('node/' . $book->nid), '%book-title' => $book->title));
  }
  if (!empty($options)) {
    $form['nodes'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Select the nodes you wish to delete'),
      '#options' => $options,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Delete selected nodes'),
    );
  }
  else {
    $form['no_nodes'] = array(
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#value' => t('There are no nodes with deleted templates.')
    );
  }
  return $form;
}

/**
 * Submit handler for deleting nodes where the associated template has also
 * been deleted.
 */
function skeleton_sync_delete_template_form_submit($form, &$form_state) {
  $batch = array(
    'title' => t('Deleting content'),
    'init_message' => t('Content deletion is starting.'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('There was an error while updating content.'),
    'file' => drupal_get_path('module', 'skeleton'). '/skeleton_sync.inc',
  );
  foreach (array_filter($form_state['values']['nodes']) as $nid) {
    $batch['operations'][] = array('skeleton_sync_delete_node', array($nid));
  }
  if (!empty($batch['operations'])) {
    batch_set($batch);
  }
}

/**
 * Update an instantiated node based on an updated template.
 *
 * @param $nid
 *   The node ID to be updated.
 *
 * @param &$context
 *   The current context of the batch operation.
 */
function skeleton_sync_update_node($nid, &$context) {
  module_load_include('inc', 'skeleton', 'skeleton_token');
  $node = node_load($nid);
  $context['message'] = t('Updating %title', array('%title' => $node->title));
  if (empty($node->skeleton_template->template_id)) {
    $context['results'][] = t('Failed updating <a href="@node-url">%title</a> as it does not have an associated template.', array('@node-url' => url('node/' . $node->nid), '%title' => $node->title));
    return;
  }
  $template = skeleton_template_load($node->skeleton_template->template_id);
  // Unset the node teaser. If a module has added it, it will be applied in
  // the foreach loop.
  unset($node->teaser);
  $template_node = (object)$template->node_data;
  // Don't change the author of nodes.
  unset($template_node->uid);
  unset($template_node->name);
  foreach ($template_node as $key => $value) {
    $node->$key = $value;
  }
  // Replace body and title tokens.
  $node = _skeleton_token_replace_values($node, 'skeleton', $node->skeleton_template->tokens);
  $node = _skeleton_token_replace_values($node, 'node', $node);

  // Add a flag so that we keep this node as "unmodified".
  $node->skeleton_template->keep_connected = TRUE;

  $node = node_submit($node);
  node_save($node);
  db_query("UPDATE {skeleton_template_node} SET template_status = 'synced' WHERE nid = %d", $node->nid);
  $context['results'][] = t('Updated %title', array('%title' => $node->title));
}

/**
 * Delete an instantiated node. This is used to delete nodes where it's
 * template has also been deleted, but could also be used to delete any node.
 *
 * @param $nid
 *   The node ID to delete.
 *
 * @param &$context
 *   The current context of the batch operation.
 */
function skeleton_sync_delete_node($nid, &$context) {
  $node = node_load($nid);
  node_delete($nid);
  $context['results'][] = t('Deleted %title', array('%title' => $node->title));
}
