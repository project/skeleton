<?php


/**
 * @file
 * Basic functions for the skeleton module
 */

include_once('skeleton_common.inc');

/**
 * Create the configuration page
 */
function skeleton_admin() {
  $result = db_query("SELECT skeleton_id, skeleton FROM {skeleton} ORDER BY skeleton_id");
  $header = array(t('Name'), t('Associated Templates'), t('Operations'));
  while ($skeleton = db_fetch_object($result)) {
    // Get the templates for this skeleton.
    $template_result = db_query("SELECT st.template_id, st.template, st.node_data, sd.parent FROM {skeleton_template} st
      INNER JOIN {skeleton_data} sd ON st.template_id = sd.template_id
      INNER JOIN {skeleton} s ON sd.skeleton_id = s.skeleton_id
      WHERE s.skeleton_id = %d
      ORDER BY sd.parent, sd.weight", $skeleton->skeleton_id);
    $templates = array();
    while ($template = db_fetch_object($template_result)) {
      $node = (object)unserialize($template->node_data);
      module_exists('translation') ? $language = ' (' . locale_language_name($node->language) . ')': $language = '';
      $templates[] = l(check_plain($template->template) . $language, 'admin/content/skeleton/template/' . $template->template_id .'/view');
    }

    $operations = array();
    if (_skeleton_user_can_create_instance()) {
      $operations[] = l(t('create new instance'), 'admin/content/skeleton/skeleton/'. $skeleton->skeleton_id . '/create');
    }
    if (user_access('configure skeleton outlines')) {
      $operations[] = l(t('edit skeleton'), 'admin/content/skeleton/skeleton/'. $skeleton->skeleton_id . '/edit');
      $operations[] = l(t('delete skeleton'), 'admin/content/skeleton/skeleton/'. $skeleton->skeleton_id . '/delete');
    }
    $rows[] = array(check_plain($skeleton->skeleton), theme('item_list', $templates), theme('item_list', $operations));
  }
  if (!empty($rows)) {
    return theme('table', $header, $rows);
  }
  else {
    return t('<p>No skeleton outlines have been created.</p>');
  }
}

/**
 * Add a new skeleton
 */
function skeleton_add() {
  $output = '';
  drupal_set_title(t('Skeleton: Add'));
  $output .= '<h3>'. t('Add New Skeleton') .'</h3>';
  $output .= drupal_get_form('skeleton_add_form');
  return $output;
}

/**
 * FormsAPI for skeleton_add()
 */
function skeleton_add_form($form_state) {
  $form = array();
  $form['skeleton'] = array(
    '#type' => 'fieldset',
    '#title' => t('Skeleton information'),
    '#collapsible' => TRUE
  );
  $form['skeleton']['skeleton'] = array(
    '#type' => 'textfield',
    '#title' => t('Skeleton name'),
    '#description' => t('Enter a unique name to identify this skeleton.'),
    '#required' => TRUE
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create skeleton'),
  );
  return $form;
}

/**
 * FormsAPI for skeleton_add()
 */
function skeleton_add_form_validate($form, &$form_state) {
  $check = db_result(db_query("SELECT COUNT(skeleton_id) FROM {skeleton} WHERE skeleton = '%s'", $form_state['values']['skeleton']));
  if ($check > 0) {
    form_set_error('skeleton', t('The name of the skeleton must be unique.'));
  }
}

/**
 * FormsAPI for skeleton_add()
 */
function skeleton_add_form_submit($form, &$form_state) {
  db_query("INSERT INTO {skeleton} (skeleton) VALUES ('%s')", $form_state['values']['skeleton']);
  // We rebuild the menu system to update the links at node/add.
  menu_rebuild();
  drupal_set_message(t('Skeleton added.'));
  $skeleton = skeleton_get_by_name($form_state['values']['skeleton']);
  drupal_goto('admin/content/skeleton/'. $skeleton->skeleton_id . '/edit');
}
