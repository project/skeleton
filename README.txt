
Skeleton Outlines
=================

== Current Maintainer ==

Andrew Berry
Email: andrewberry@sentex.net
Drupal: deviantintegral
CVS: deviantintegral
IRC: deviantintegral

== Original Author ==

Ken Rickard
Email: agentrickard [at] gmail [dot] com
Drupal: agentrickard
CVS: agentken
IRC: agentrickard


Contents
========

1.  Overview
2.  Requirements
3.  Installation
4.  Permissions
5.  Usage
6.  To Do

--------
1. Overview

The Skeleton module works with the core Book module to create pre-configured outlines that can
be reused.  After creating a skeleton and assigning templates to it, users with the proper permission
will be able to create new books that clone the outline and content of the skeleton.

The original use-case of this module is for publishing on an intranet site, where it is important to create
the same information structure repeatedly.  The skeleton module is designed to make this repetitive task
easy to perform -- especially for non-technical site administrators.

Sample skeleton: Create customer record keeping for Drupal development.

  Page              Node Type       Parent          Weight   Visibility
  ----              ---------       -----           ------   ---------
  Project Summary   CCK             none            -15      All users
  Deadlines         CCK (dates)     Project Summary -10      Staff 
  Customer FAQ      forum           Project Summary   0      Staff && Customer
  Contracts         book (files)    none              3      Management && Customer
  Project Notes     forum           none              5      Staff
  Documentation     book            none             10      All users
  Technical Docs    book            Documentation     0      Staff
  End User Docs     book            Documentation     5      All users
  Screenshots       book            End User Docs     0      All users

This way, you set up the skeleton rules once, including all default node data and settings.  
Then you simply create a new "instance" of the book for each customer.

The Token module is used to allow for tokenized replacements within Title, Body, and CCK Text
fields. Tokens can be defined by site administrators. When a custom token is used within a
template, users are asked to fill in a value for that token when creating a skeleton instance.
This feature is best used for snippets of text within node bodies; for more complicated data,
consider if a CCK field would be a better fit.

Skeletons templates can also be syncronized to existing content, provided that content hasn't
been modified. Once the content has been modified, future changes to the template are always
ignored to prevent overwriting customized content. Syncronization supports adding or deleting
templates, as well as the content within templates.

--------
2. Requirements

The Skeleton module requires that the Book module is enabled and configured.  Book is a core Drupal
module. The Token (http://drupal.org/project/token) module is also required.

The current development release is dependent on patches from two core issues:

  * http://drupal.org/node/364529: menu_tree_all_data() static cache can become stale
  * http://drupal.org/node/360377: book_get_books() cache becomes stale when batch-inserting book pages.

You must apply the latest Drupal 6 patches from those issues for this module to work. For information
about patching, please see the patch documentation at http://drupal.org/patch.

--------
3. Installation

Skeleton uses the standard Drupal install system. Extract the module to your modules directory
(usually sites/all/modules) and enable it on the modules page.

Skeleton can be cleanly uninstalled by using the module Uninstall tab on the modules overview page.

--------
4. Permissions

There are two permission settings (Access Rules) for the Skeleton module.

  "configure skeleton outlines" -- this is the admin privilege; it allows users to create, edit, syncronize,
    and delete skeletons and templates.  At least one user must have this permission.
  
  "create new instances" -- an 'instance' is a new book created from a skeleton.  Users with this
    privilege can only create new books from existing skeletons.  This permission is designed for
    non-technical users.

--------
5. Usage

After configuring module permissions, you must create at least one skeleton and at least one template.

When you have created template(s) and skeleton(s), you will be able to assign templates to skeletons,
set the outline order, and create new instances.

Templates may be of any registered node type.  When you create a template, you will first be asked
to select the node type.  After this step, you will be taken to the template editing form.

When editing a template, you will be presented with a node editing form. Skeleton uses hook_form_alter() 
to present a modified node form for storing template data.

The Skeleton module works with Book module, Node and CCK modules, and the Node Access module -- 
and in theory it should work with all node modules.  It has been tested with Node Access expressly, which
allows you to configure the visibility of individual pages within your skeleton.

Note that the following node form elements cannot be accessed by the Skeleton module (see line 226 of
skeleton_template.inc):
  
    'menu', 'path', 'attachments', 'field_file', 'log', 'preview'
    
Attachments via upload.module and CCK's filefield are expressly prohibited.      

--------
6. To Do

This module is in ALPHA release stage and it needs some developer love.  Some issues to test and work on:

  a) skeleton_get_tree() needs review.
      This function is cloned from taxonomy_get_tree() and is uesd to prepare the outline schema for
      a skeleton outline.  There are likely efficiencies to be gained by optimizing this function.
      
      skeleton_get_tree() is in skeleton_common.inc

For all other issues, please see the issue queue at http://drupal.org/project/issues/skeleton.      
   