<?php


/**
 * @file
 *   Template creation functions for the skeleton module
 */

include_once('skeleton_common.inc');

/**
 * List all templates
 *
 * @param $template_id
 *   Unused, sent by the router function
 * @return
 *   A table of current templates or an error message.
 */
function skeleton_list_template($template_id) {
  drupal_set_title(t('Skeleton templates'));
  $result = db_query("SELECT * FROM {skeleton_template} ORDER BY template_id");
  $header = array(t('Name'), t('Node type'), t('Associated skeletons'));
  if (module_exists('translation')) {
    $header[] = t('Language');
  }
  $header[] = t('Operations');
  while ($template = db_fetch_object($result)) {
    $skeleton_result = db_query("SELECT s.skeleton_id, s.skeleton FROM {skeleton} s
      INNER JOIN {skeleton_data} sd ON s.skeleton_id = sd.skeleton_id
      INNER JOIN {skeleton_template} st ON st.template_id = sd.template_id
      WHERE st.template_id = %d
      ORDER BY s.skeleton_id", $template->template_id);
    $skeletons = array();
    while ($skeleton = db_fetch_object($skeleton_result)) {
      $skeletons[] = l(check_plain($skeleton->skeleton), 'admin/content/skeleton/skeleton/' . $skeleton->skeleton_id . '/edit');
    }
    $extra = l(t('view template'), 'admin/content/skeleton/template/' . $template->template_id . '/view');
    $extra2 = l(t('edit template'), 'admin/content/skeleton/template/'. $template->template_id . '/edit');
    $extra3 = l(t('delete template'), 'admin/content/skeleton/template/'. $template->template_id . '/delete');
    $operations = theme('item_list', array($extra, $extra2, $extra3));
    $row = array(
      check_plain($template->template),
      $template->node_type,
      theme('item_list', $skeletons),
    );
    if (module_exists('translation')) {
      $node = (object)unserialize($template->node_data);
      $row[] = locale_language_name($node->language);
    }
    $row[] = $operations;
    $rows[] = $row;
  }
  if (!empty($rows)) {
    return theme('table', $header, $rows);
  }
  else {
    return t('<p>No skeleton templates have been created.</p>');
  }
}

/**
 * View a store template.
 * @param $template_id
 *   The ID of the template to view.
 * @return
 *   The fully rendered node HTML.
 */
function skeleton_view_template($template) {
  if (empty($template->node_data)) {
    drupal_set_title(check_plain($template->template));
    return '<p>' . t('This template has no content set. Please <a href="@edit-template">add some content</a> to the template.', array('@edit-template' => url('admin/content/skeleton/template/' . $template->template_id . '/edit'))) . '</p>';
  }
  $node = (object)$template->node_data;
  drupal_set_title($node->title);
  return node_view($node, FALSE, TRUE);
}

/**
 * Add new template
 *
 * @param $template_id
 *   The id of the template to act upon
 * @return
 *   A themed HTML page, including the form
 */
function skeleton_add_template($template_id) {
  drupal_set_title(t('Skeleton templates: Add'));
  return drupal_get_form('skeleton_add_template_form', $template_id);
}

/**
 * FormsAPI for skeleton_add_template()
 */
function skeleton_add_template_form($form_state) {
  $form = array();
  $form['skeleton_template'] = array(
    '#type' => 'fieldset',
    '#title' => t('Template information'),
    '#collapsible' => TRUE
  );
  $form['skeleton_template']['template'] = array(
    '#type' => 'textfield',
    '#title' => t('Template name'),
    '#description' => t('Enter a unique name to identify this template.'),
    '#required' => TRUE
  );
  $types = node_get_types();
  foreach ($types as $type) {
    $options[$type->type] = $type->name;
  }
  $form['skeleton_template']['node_type'] = array(
    '#type' => 'select',
    '#title' => t('Node type'),
    '#options' => $options,
    '#description' => t('Select the node type for this template.'),
    '#required' => TRUE
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create template')
  );
  // TODO merge this form with the edit form?
  return $form;
}

/**
 * FormsAPI for skeleton_add_template()
 */
function skeleton_add_template_form_validate($form, &$form_state) {
  $check = db_result(db_query("SELECT COUNT(template_id) FROM {skeleton_template} WHERE template = '%s'", $form_state['values']['template']));
  if ($check > 0) {
    form_set_error('template', t('The name of the template must be unique.'));
  }
}

/**
 * FormsAPI for skeleton_add_template()
 */
function skeleton_add_template_form_submit($form, &$form_state) {
  db_query("INSERT INTO {skeleton_template} (template, node_type) VALUES ('%s', '%s')", $form_state['values']['template'], $form_state['values']['node_type']);
  drupal_set_message(t('Template added.'));
  $template = skeleton_get_template_by_name($form_state['values']['template']);
  drupal_goto('admin/content/skeleton/template/'. $template->template_id . '/edit');
}

/**
 * Delete template
 *
 * @param $template
 *   The template to act upon
 * @return
 *   A themed HTML page, including the form
 */
function skeleton_delete_template(&$form_state, $template) {
  $form['template_id'] = array(
    '#type' => 'value',
    '#value' => $template->template_id,
  );
  return confirm_form($form,
    t('Are you sure you want to delete the template %title?', array('%title' => $template->template)),
    'admin/content/skeleton/template',
    t('This action cannot be undone.'),
    t('Delete template'),
    t('Cancel')
  );
}

/**
 * Redirect when clicking delete button on template form.
 */
function skeleton_template_delete_redirect($form_id, &$form_state) {
  $form_state['redirect'] = 'admin/content/skeleton/template/' . $form_state['values']['template_id'] . '/delete';
}

/**
  * FormsAPI for skeleton_delete_template()
  */
function skeleton_delete_template_submit($form, &$form_state) {
  db_query("DELETE from {skeleton_template} WHERE template_id = %d", $form_state['values']['template_id']);
  db_query("UPDATE {skeleton_data} SET parent = 0 WHERE parent = %d", $form_state['values']['template_id']);
  db_query("DELETE from {skeleton_data} WHERE template_id = %d", $form_state['values']['template_id']);
  drupal_set_message(t('Template deleted.'));
  drupal_goto('admin/content/skeleton/template');
}

/**
 * Edit template
 *
 * @param $template
 *   The template to act upon
 * @return
 *   A themed HTML page, including the form
 */
function skeleton_edit_template($template) {
  include_once drupal_get_path('module', 'node') . '/node.pages.inc';
  drupal_set_title($template->node_data['title']);
  $check = FALSE;
  if (!empty($template->node_data)) {
    $node = $template->node_data;
    $node['type'] = $template->node_type;
    $check = TRUE;
  }
  else {
    $node = array('uid' => $user->uid, 'name' => $user->name, 'type' => $template->node_type);
  }
  $node_form = $template->node_type .'_node_form';
  return drupal_get_form($node_form, $node);
}

/**
 * Alter the node edit form to make it suitable for a skeleton template.
 *
 * @param $form
 *   The form array to alter.
 * @param $form_state
 *   The state of the current form.
 * @param $form_id
 *   The ID of the current form, usually node-type_node_form.
 */
function skeleton_alter_node_form(&$form, $form_state, $form_id) {
  $template = skeleton_template_load(arg(4));
  if (!empty($template->node_data)) {
    $node = $template->node_data;
  }
  // handles some form elements (radios mostly) that don't get set correctly
  foreach ($form as $key => $value) {
    if (isset($node[$key]['key']) && isset($form[$key]['key']['#default_value'])) {
      $form[$key]['key']['#default_value'] = $node[$key]['key'];
    }
  }
  // publishing options are handled oddly -- this may affect all checkbox types
  foreach ($form['options'] as $key => $value) {
    if (isset($node[$key])) {
      $form['options'][$key]['#default_value'] = $node[$key];
    }
  }
  // handle taxonomies -- this may apply to all select elements
  if (!empty($form['taxonomy'])) {
    foreach ($form['taxonomy'] as $key => $value) {
      if (is_numeric($key)) {
        $form['taxonomy'][$key]['#default_value'] = $node['taxonomy'][$key];
      }
    }
  }
  $form['template_id'] = array('#type' => 'value', '#value' => $template->template_id);
  $form['type'] = array('#type' => 'value', '#value' => $template->node_type);
  $form['skeleton_template'] = array(
    '#type' => 'fieldset',
    '#title' => t('Template information'),
    '#collapsible' => TRUE,
    '#weight' => -100
  );
  $form['skeleton_template']['template'] = array(
    '#type' => 'textfield',
    '#title' => t('Template name'),
    '#default_value' => $template->template,
    '#description' => t('The unique name to identify this template.'),
    '#required' => TRUE
  );
  $options = node_get_types('names');
  $form['skeleton_template']['node_type'] = array(
    '#type' => 'select',
    '#title' => t('Node type'),
    '#value' => $template->node_type,
    '#options' => $options,
    '#description' => t('The node type for this template. <em>This value cannot be edited.</em>'),
    '#disabled' => TRUE
  );

  // Enable translatable template support.
  if (module_exists('translation') && translation_supported_type($template->node_type)) {
    module_load_include('inc', 'skeleton', 'skeleton_translate');
    _skeleton_translate_template_form($form, $form_state, $form_id, $template, $node);
  }

  $form['skeleton_template']['template_id'] = array('#type' => 'value', '#value' => $template->template_id);

  // Add token help information below the skeleton fieldset.
  $form['skeleton_tokens'] = array(
    '#type' => 'fieldset',
    '#title' => t('Skeleton replacement tokens'),
    '#description' => t('These tokens will be replaced in the title and the body when instantiating a skeleton outline'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => -99,
  );
  $form['skeleton_tokens']['tokens'] = array(
    '#value' => _skeleton_build_token_help('skeleton'),
  );
  $form['node_tokens'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node replacement tokens'),
    '#description' => t('These tokens will be replaced in the title and the body when instantiating a skeleton outline'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => -98,
  );
  $form['node_tokens']['tokens'] = array(
    '#value' => _skeleton_build_token_help('node'),
  );

  // Unset the values we don't want or cannot save.
  $unset = array('parent', 'weight', 'menu', 'book', 'path', 'attachments', 'revision_information', 'log', 'buttons', '#submit');

  // Saving Filefield (or Imagefield) fields as is really breaks things, so
  // find any such fields and remove them.
  $content_type = content_types($form['#node']->type);
  foreach ($content_type['fields'] as $data) {
    if ($data['type'] == 'filefield') {
      $unset[] = $data['field_name'];
    }
  }
  foreach ($unset as $item) {
    unset($form[$item]);
  }

  if (module_exists('nodeaccess') && user_access('grant node permissions')) {
    $form['skeleton_template_grants'] = array(
      '#type' => 'fieldset',
      '#title' => t('Node access rules'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => 100,
      '#theme' => 'skeleton_na_form_element'
    );
    // get roles and set default permissions
    $roles = user_roles();
    if (isset($node['rid'])) {
      // set the gid into the array correctly
      $perms = $node['rid'];
      foreach ($roles as $key => $value) {
        $perms[$key] = $node['rid'][$key];
        $perms[$key]['gid'] = $key;
        $perms[$key]['name'] = $roles[$key];
      }
    }
    else {
      $perms = variable_get('nodeaccess_'. $template->node_type, array());
    }
    foreach ($perms as $perm) {
      $name = $roles[$perm['gid']];
      $roles[$perm['gid']] = $perm;
      $roles[$perm['gid']]['name'] = $name;
    }
    $naform = array();
    if (is_array($roles)) {
      $naform['rid'] = array('#tree' => TRUE);
      $allowed = variable_get('nodeaccess-roles', array());
      foreach ($roles as $key => $field) {
        if ($allowed[$key]) {
          $naform['rid'][$key]['name'] = array('#type' => 'hidden', '#value' => $field['name']);
          $naform['rid'][$key]['grant_view'] = array('#type' => 'checkbox', '#default_value' => $field['grant_view']);
          $naform['rid'][$key]['grant_update'] = array('#type' => 'checkbox', '#default_value' => $field['grant_update']);
          $naform['rid'][$key]['grant_delete'] = array('#type' => 'checkbox', '#default_value' => $field['grant_delete']);
        }
      }
    }
    if (count($naform['rid']) > 1) {
      $form['skeleton_template_grants']['na'] = $naform;
    }
    else {
      unset($form['skeleton_template_grants']);
    }
  }
  $form['buttons']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete template'),
    '#weight' => 200,
    '#submit' => array('skeleton_template_delete_redirect'),
  );
  $form['#submit'][] = 'skeleton_edit_template_form_submit';
  $form['#validate'][] = 'skeleton_edit_template_form_validate';
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update template'),
    '#weight' => 100
  );
}

/**
 * Theme the nodeaccess part of the form
 */
function theme_skeleton_na_form_element($form) {
  // role table
  $roles = element_children($form['na']['rid']);
  if (count($roles) > 0) {
    $header = array(t('Role'), t('View'), t('Edit'), t('Delete'));
    foreach ($roles as $key) {
      $row = array();
      $row[] = $form['na']['rid'][$key]['name']['#value'] . drupal_render($form['rid'][$key]['name']);
      $row[] = drupal_render($form['na']['rid'][$key]['grant_view']);
      $row[] = drupal_render($form['na']['rid'][$key]['grant_update']);
      $row[] = drupal_render($form['na']['rid'][$key]['grant_delete']);
      $rows[] = $row;
    }
    $output .= theme('table', $header, $rows);
  }
  return $output;
}

/**
 * FormsAPI
 */
function skeleton_edit_template_form_validate($form, &$form_state) {
  if (module_exists('translation') && isset($form['skeleton_template']['translations'])) {
    module_load_include('inc', 'skeleton', 'skeleton_translate');
    _skeleton_translate_template_form_validate($form, $form_state);
  }
}

/**
 * FormsAPI for handling the template form
 */
function skeleton_edit_template_form_submit($form, &$form_state) {
  if (module_exists('translation') && isset($form['skeleton_template']['translations'])) {
    module_load_include('inc', 'skeleton', 'skeleton_translate');
    _skeleton_translate_template_form_submit($form, $form_state);
  }

  // these items are not stored node data
  $node = $form_state['values'];
  $unset = array('template', 'node_type', 'nid', 'vid', 'teaser_js', 'created', 'changed', 'date', 'op', 'submit', 'form_token', 'form_id', '#after_build');
  foreach ($unset as $item) {
    unset($node[$item]);
  }
  db_query("UPDATE {skeleton_template} SET template = '%s', node_data = '%s' WHERE template_id = %d", $form_state['values']['template'], serialize($node), $form_state['values']['template_id']);
  drupal_set_message(t('Template successfully updated.'));

  // Now, mark any instantiated copies of this template as eligible for
  // updating.
  db_query("UPDATE {skeleton_template_node} SET template_status = 'updated' WHERE template_id = %d AND template_status != 'overridden'", $form_state['values']['template_id']);
}

/**
 * Add a template to a skeleton
 *
 * @param $action
 *   Route the function to the correct action (add / remove)
 * @param $skeleton_id
 *   The id of the template to act upon
 * @param $template_id
 *   The id of the template to act upon
 * @return
 *   Callback function, will return you to the current page after acting.
 */
function skeleton_assign_template($action, $skeleton_id, $template_id) {
  if (($action != 'add' && $action != 'remove') || intval($skeleton_id) == 0 || intval($template_id) == 0) {
    return t('Illegal request');
  }
  include_once('skeleton_instance.inc');
  $skeleton = skeleton_load($skeleton_id);
  $template = skeleton_template_load($template_id);
  return skeleton_assign($action, $skeleton, $template);
}
