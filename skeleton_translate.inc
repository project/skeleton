<?php


/**
 * @file
 * Functions for handling the translation of skeleton templates. In general,
 * this file should only be loaded if translate.module is enabled. 
 */

/**
 * Load all translations of a given source template, including the source
 * template itself.
 *
 * @param $template
 *   A fully loaded template object.
 *
 * @return
 *   An array of template objects keyed by the language of the template.
 */
function skeleton_template_load_translations($template) {
  if ($template->ttid == 0) {
    return array();
  }
  $result = db_query("SELECT template_id FROM {skeleton_template} WHERE ttid = %d", $template->ttid);
  $translations = array($template->language => $template);
  while ($translation_id = db_result($result)) {
    // We could do a SELECT * and reduce the number of DB queries, but that
    // would require replicating the code in the loader function, which could
    // be a pain down the line.
    $translation = skeleton_template_load($translation_id);
    $translations[$translation->language] = $translation;
  }
  return $translations;
}

/**
 * Display a list of all books to propagate translation mappings to. Each
 * submitted set of checkboxes should correspond to a set of translation sets,
 * with each node in a set belonging to a different book.
 */
function skeleton_sync_translation_form($form_state) {
  $form = array();
  $books = book_get_books();
  $book_options = array();
  foreach($books as $bid => $book) {
    $book_options[$bid] = $book['title'];
  }
  $form['books'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Syncronize translations within these books'),
    '#options' => $book_options,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update translations'),
  );
  return $form;
}

/**
 * FormsAPI validation hook for the translation sync form. The user must select
 * at least two books, and every selected book must be of a different language.
 * Drupal prevents nodes of different languages from being in the same book,
 * greatly simplifying the validation.
 */
function skeleton_sync_translation_form_validate($form, &$form_state) {
  $selected_books = array_filter($form_state['values']['books']);
  if (count($selected_books) < 2) {
    form_set_error('books', t('Select at least 2 books in different languages.'));
  }
  $languages = array();
  foreach ($selected_books as $book) {
    $book = node_load($book);
    if (!in_array($book->language['language'], $languages)) {
      $languages[] = $book->language['language'];
    }
    else {
      form_set_error('books', t('Multiple books of the same language were selected.'));
    }
  }
}

/**
 * FormsAPI submit handler for the translation propagation form. On submit,
 * the form finds every node within each book which was instantiated from a
 * skeleton template. These templates are then built into their associated
 * template translation sets, which are used to calculate a translation source
 * for each node.
 *
 * TODO: This is probably a good candidate for a Batch API function.
 */
function skeleton_sync_translation_form_submit($form, &$form_state) {
  $translations = array();
  $selected_books = array_filter($form_state['values']['books']);
  foreach ($selected_books as $bid) {
    $result = db_query("SELECT b.nid, stn.template_id, st.ttid FROM {book} b INNER JOIN {skeleton_template_node} stn ON b.nid = stn.nid INNER JOIN {skeleton_template} st ON stn.template_id = st.template_id WHERE bid = %d", $bid);
    while ($row = db_fetch_object($result)) {
      // Exclude templates not in a translation set.
      if ($row->ttid > 0) {
        $translations[$row->ttid][$row->template_id] = node_load($row->nid);
      }
    }
  }
  foreach ($translations as $ttid => $set) {
    // Sanity check to ensure that the set has more then one template
    // associated.
    if (count($set) > 1) {
      $tnid = $set[$ttid]->nid;
      foreach ($set as $ttid => $node) {
        db_query("UPDATE {node} SET tnid = %d WHERE nid = %d AND tnid = 0", $tnid, $node->nid);
      }
    }
  }
  drupal_set_message(t("Template translations have been saved."));
}

/**
 * Generates 'title [nid:$nid]' for the autocomplete field.
 */
function skeleton_template_tid2autocomplete($template) {
  return check_plain($template->template) . ' [tid:' . $template->template_id .']';
}

/**
 * Reverse mapping from template title to tid.
 *
 * We also handle autocomplete values (title [tid:x]) and validate the form
 */
function skeleton_template_autocomplete2nid($name, $field = NULL, $type, $language) {
  if (!empty($name)) {
    preg_match('/^(?:\s*|(.*) )?\[\s*tid\s*:\s*(\d+)\s*\]$/', $name, $matches);
    if (!empty($matches)) {
      // Explicit [tid:n].
      list(, $title, $ttid) = $matches;
      if (!empty($title) && ($template = skeleton_template_load($ttid)) && $title != $template->template) {
        if ($field) {
          form_set_error($field, t('Template title mismatch. Please check your selection.'));
        }
        $ttid = NULL;
      }
    }
    else {
      // No explicit tid.
      $reference = _skeleton_template_references($name, 'equals', array('node_type' => $type, 'language' => $language), 1);
      if (!empty($reference)) {
        $ttid = key($reference);
      }
      elseif ($field) {
        form_set_error($field, t('Found no valid template with that title: %title', array('%title' => $name)));
      }
    }
  }
  return !empty($ttid) ? $ttid : NULL;
}

/**
 * FormsAPI alter function to enable template translations. Much of this is
 * from the i18n module.
 *
 * @param $form
 *   The form array from hook_form_alter().
 *
 * @param $form_state
 *   The form state from hook_form_alter().
 *
 * @param $form_id
 *   The form id from hook_form_alter().
 *
 * @param $template
 *   The current skeleton template being edited.
 *
 * @param $node
 *   The representation of the node data for the current template as an array.
 */
function _skeleton_translate_template_form(&$form, $form_state, $form_id, $template, $node) {
  // The translation set ID is either set, so load the source translation, or
  // this template will be the new source translation.
  $template->ttid != 0 ? $source_translation = skeleton_template_load($template->ttid) : $source_translation = $template;

  $form['skeleton_template']['source_translation'] = array(
    '#type' => 'hidden',
    '#value' => $source_translation->ttid,
  );

  $translations = skeleton_template_load_translations($source_translation);

  // If there is a translation for this template, ensure that the user doesn't
  // select a translated language for this template.
  if (isset($form['language']['#options'])) {
    foreach (array_keys($translations) as $language) {
      if ($language != $template->node_data['language']) {
        unset($form['language']['#options'][$language]);
      }
    }
  }

  $form['skeleton_template']['translations'] = array(
    '#type' => 'fieldset',
    '#title' => t('Select translations for %title', array('%title' => $source_translation->template)),
    '#tree' => TRUE,
    '#theme' => 'skeleton_template_select_translation',
    '#description' => t("You can select existing templates as translations of this one or remove templates from this translation set. Only templates that have the right language and don't belong to another translation set will be available here.")
  );

  // Build the language autocomplete dropdowns.
  foreach (language_list() as $language) {
    if ($language->language != $template->node_data['language']) {
      $trans_tid = isset($translations[$language->language]) ? $translations[$language->language]->template_id : 0;
      $form['skeleton_template']['translations']['ttid'][$language->language] = array(
        '#type' => 'value',
        '#value' => $trans_tid
      );

      // Special formating for the source language label.
      if (!empty($trans_tid) && $trans_tid == $template->ttid) {
        $form['skeleton_template']['translations']['language'][$language->language] = array(
          '#value' => t('<strong>@language_name</strong> (source)', array('@language_name' => $language->name))
        );
      }
      else {
        $form['skeleton_template']['translations']['language'][$language->language] = array(
          '#value' => $language->name
        );
      }

      $form['skeleton_template']['translations']['template'][$language->language] = array(
        '#type' => 'textfield',
        '#autocomplete_path' => 'skeleton/template/autocomplete/' . $source_translation->node_type . '/' . $language->language,
        '#default_value' => $trans_tid ? skeleton_template_tid2autocomplete($translations[$language->language]) : '',
      );
    }
  }

  // The "retranslate all translations" checkbox, or the "needs updating" flag.
  if ($form['skeleton_template']['source_translation']['#value'] > 0) {
    if ($template->template_id == $template->ttid) {
      $form['skeleton_template']['translations']['translate_retranslate'] = array(
        '#type' => 'checkbox',
        '#title' => t('Flag translations as outdated'),
        '#description' => t('If you made a significant change, which means translations should be updated, you can flag all translations of this post as outdated. This will not change any other property of those posts, like whether they are published or not.'),
      );
    }
    else {
      $form['skeleton_template']['translations']['translate_status'] = array(
        '#type' => 'checkbox',
        '#title' => t('This translation needs to be updated'),
        '#description' => t('When this option is checked, this translation needs to be updated because the source post has changed. Uncheck when the translation is up to date again.'),
        '#default_value' => $template->translate,
      );
    }
  }
}

/**
 * Validation handler for the translation portion of the template form.
 */
function _skeleton_translate_template_form_validate($form, &$form_state) {
  foreach ($form_state['values']['translations']['template'] as $lang => $title) {
    if (!$title) {
      $ttid = 0;
    }
    else {
      $ttid = skeleton_template_autocomplete2nid($title, "translations][template][$lang", array($template->node_type), array($lang));
    }
    $form_state['values']['translations']['ttid'][$lang] = $ttid;
  }
}

/**
 * Submit handler for the translation portion of the template form.
 */
function _skeleton_translate_template_form_submit($form, &$form_state) {
  $node = $form_state['values'];

  // Save the language of the node for easy access.
  if (isset($node['language'])) {
    db_query("UPDATE {skeleton_template} SET language = '%s' WHERE template_id = %d", $node['language'], $form_state['values']['template_id']);
  }

  // Get all of the current translations in the database.
  $translations = skeleton_template_load_translations(skeleton_template_load($form_state['values']['template_id']));
  $current_translations = array();
  foreach ($translations as $trans) {
    $current_translations[$trans->language] = $trans->template_id;
  }
  $update = array($node['language'] => $form_state['values']['template_id']) + array_filter($form_state['values']['translations']['ttid']);

  // Compute the difference to see which are the new translations and which ones to remove.
  $new = array_diff_assoc($update, $current_translations);
  $remove = array_diff_assoc($current_translations, $update);

  // The tricky part: If the existing source is not in the new set, we need to create a new ttid.
  if (array_search($form_state['values']['ttid'], $update)) {
    $ttid = $form_state['values']['source_translation'];
    $add = $new;
  }
  else {
    // Create new ttid, which is the source template.
    $ttid = $form_state['values']['template_id'];
    $add = $update;
  }

  // Now update values for all templates.
  if ($add) {
    $args = array('' => $ttid) + $add;
    db_query('UPDATE {skeleton_template} SET ttid = %d WHERE template_id IN (' . db_placeholders($add) . ')', $args);
    if (count($new)) {
      drupal_set_message(format_plural(count($new), 'Added a template to the translation set.', 'Added @count templates to the translation set.'));
    }
  }

  if ($remove) {
    if (count($update) == 1) {
      $remove += array($node['language'] => $form_state['values']['template_id']);
    }
    db_query('UPDATE {skeleton_template} SET ttid = 0 WHERE template_id IN (' . db_placeholders($remove) . ')', $remove);
    drupal_set_message(format_plural(count($remove), 'Removed a template from the translation set.', 'Removed @count templates from the translation set.'));
  }

  // Flag all translations as needing to be updated.
  if (!empty($node['translations']['translate_retranslate'])) {
    foreach ($translations as $trans) {
      if ($trans->template_id != $trans->ttid) {
        db_query("UPDATE {skeleton_template} SET translate = 1 WHERE template_id = %d", $trans->template_id);
      }
    }
  }

  // Flag for this translation only.
  if (!empty($node['translations']['translate_status'])) {
    db_query("UPDATE {skeleton_template} SET translate = 1 WHERE template_id = %d", $form_state['values']['template_id']);
  }
  else {
    db_query("UPDATE {skeleton_template} SET translate = 0 WHERE template_id = %d", $form_state['values']['template_id']);
  }

  unset($form_state['values']['translations']);
}

/**
 * Theme select translation form
 * @ingroup themeable
 */
function theme_skeleton_template_select_translation(&$elements) {
  $output = '';
  if (isset($elements['ttid'])) {
    $rows = array();
    foreach (element_children($elements['ttid']) as $lang) {
      $rows[] = array(
        drupal_render($elements['language'][$lang]),
        drupal_render($elements['template'][$lang]),
      );
    }
    $output .= theme('table', array(), $rows);
    $output .= drupal_render($elements);
  }
  return $output;
}

/**
 * Template title autocomplete callback.
 */
function skeleton_template_autocomplete($type, $language, $string = '') {
  $params = array('node_type' => $type, 'ttid' => 0, 'language' => $language);
  $matches = array();
  foreach (_skeleton_template_references($string, 'contains', $params) as $id => $row) {
    // Add a class wrapper for a few required CSS overrides.
    $matches[$row['title'] ." [tid:$id]"] = '<div class="reference-autocomplete">'. $row['rendered'] . '</div>';
  }
  drupal_json($matches);
}

/**
 * Find template title matches.
 *
 * @param $string
 *   String to match against node title
 * @param $match
 *   Match mode: 'contains', 'equals', 'starts_with'
 * @param $params
 *   Other query arguments: type, language or numeric ones
 *
 * Some code from CCK's nodereference.module
 */
function _skeleton_template_references($string, $match = 'contains', $params = array(), $limit = 10) {
  $where = $args = array();
  $match_operators = array(
    'contains' => "LIKE '%%%s%%'",
    'equals' => "= '%s'",
    'starts_with' => "LIKE '%s%%'",
  );
  foreach ($params as $key => $value) {
    $type = in_array($key, array('node_type', 'language')) ? 'char' : 'int';
    if (is_array($value)) {
      $where[] = "t.$key IN (" . db_placeholders($value, $type) . ') ';
      $args = array_merge($args, $value);
    }
    else {
      $where[] = "t.$key = " . db_type_placeholder($type);
      $args[] = $value;
    }
  }
  $where[] = 't.template '. (isset($match_operators[$match]) ? $match_operators[$match] : $match_operators['contains']);
  $args[] = $string;
  $sql = 'SELECT t.template_id, t.template, t.node_type FROM {skeleton_template} t WHERE ' . implode(' AND ', $where) . ' ORDER BY t.template, t.node_type';
  $result = db_query_range($sql, $args, 0, $limit) ;
  $references = array();
  while ($template = db_fetch_object($result)) {
    $references[$template->template_id] = array(
      'title' => $template->template,
      'rendered' => check_plain($template->template),
    );
  }
  return $references;
}
