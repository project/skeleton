<?php


/**
 * Form API callback for Tokens tab
 */
function skeleton_token_form() {
  $form = array();
  $form['skeleton_tokens_help'] = array(
    '#prefix' => '<p>',
    '#suffix' => '</p>',
    '#value' => t('Skeleton tokens allows the use of tokens to automatically customize instantiated templates. For example, the [site-mail] token will insert the site administrative email address. Custom tokens added on this page will be presented in a form to fill in values when instantiating a template.'),
  );
  $form['skeleton_tokens'] = array(
    '#type' => 'markup',
    '#value' => _skeleton_build_token_help('skeleton'),
  );
  $form['tokens_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node tokens'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['tokens_fieldset']['tokens'] = array(
    '#type' => 'markup',
    '#value' => _skeleton_build_token_help('node'),
  );
  return $form;
}

/**
 * Implementation of theme function for the skeleton token page.
 *
 * @param $form
 *   The Form API array to display
 * @return
 *   The themed HTML.
 */
function theme_skeleton_token_form($form) {
  $output .= drupal_render($form);
  return $output;
}

/**
 * Forms API for the add token form. This allows new skeleton tokens to be
 * added and existing tokens to be edited.
 *
 * @param $form_id
 *   The form_id of the current form.
 * @param $token
 *   The token object to edit, or NULL if adding a new token.
 * @return
 *   Form API form array.
 */
function skeleton_add_token_form($form_id, $token = NULL) {
  $form = array();
  $form['token_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Token name'),
    '#description' => t('Enter the name of a new token to create. Do not include the enclosing brackets. Only letters, numbers, and dashes are allowed.'),
    '#default_value' => empty($token) ? "" : $token->token,
    '#maxlength' => 80,
    '#required' => TRUE,
  );
  $form['token_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Token description'),
    '#description' => t('Enter a description for this token describing what it is for.'),
    '#default_value' => empty($token) ? "" : check_plain($token->description),
    '#maxlength' => 80,
    '#required' => TRUE,
  );
  if (!empty($token)) {
    $form['token_id'] = array(
      '#type' => 'value',
      '#value' => $token->token_id,
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );
  $form['#redirect'] = 'admin/content/skeleton/token';
  return $form;
}

/**
 * Validation function for skeleton_add_token_form.
 * 
 * We only allow letters, numbers, and dashes in the token name.
 * 
 * @param $form
 *   The form being validated.
 * @param $form_state
 *   The current state of skeleton_add_token_form.
 */
function skeleton_add_token_form_validate($form, &$form_state) {
  if (!($form_state['values']['op'] == t('Cancel'))) {
    if (!preg_match('!^[a-zA-Z0-9\-]+$!', $form_state['values']['token_name'])) {
      form_set_error('token_name', t('Only letters, numbers, and dashes are allowed.'));
    }
  }
}

/**
 * Submit function for skeleton_add_token_form.
 * 
 * Adds or updates a token to the database.
 * 
 * @param $form
 *   The form being submitted.
 * @param $form_state
 *   The current state of the skeleton_add_token_form.
 */
function skeleton_add_token_form_submit($form, &$form_state) {
  if (!($form_state['values']['op'] == t('Cancel'))) {
    $token = new stdClass();
    $token->token = $form_state['values']['token_name'];
    $token->description = $form_state['values']['token_description'];
    if ($form_state['values']['token_id']) {
      $token->token_id = $form_state['values']['token_id'];
      db_query("UPDATE {skeleton_token} SET token = '%s', description = '%s' WHERE token_id = %d", $token->token, $token->description, $token->token_id);
    }
    else {
      db_query("INSERT INTO {skeleton_token} (token, description) VALUES('%s', '%s')", $token->token, $token->description);
    }
  }
}

/**
 * Form API for the delete token form.
 * 
 * @param $form_id
 *   The form_id of the current form.
 * @param $token
 *   The token object being deleted.
 * @return
 *   Form API array of the deletion form.
 */
function skeleton_delete_token_form($form_id, $token) {
  $form['token_id'] = array(
    '#type' => 'value',
    '#value' => $token->token_id,
  );
  return confirm_form($form,
    t('Are you sure you want to delete the token %token?', array('%token' => $token->token)),
    'admin/content/skeleton/token',
    t('This action cannot be undone.'),
    t('Delete token'),
    t('Cancel')
  );
}

/**
 * Submit function for skeleton_delete_token_form.
 * 
 * @param $form
 *   The current form.
 * @param $form_state
 *   The state of the current form.
 */
function skeleton_delete_token_form_submit($form, &$form_state) {
  db_query("DELETE from {skeleton_token} WHERE token_id = %d", $form_state['values']['token_id']);
  drupal_set_message(t('Token deleted.'));
  drupal_goto('admin/content/skeleton/token');
}

/**
 * Implementation of hook_token_list.
 *
 * @param $type
 *   The type of token list to return. Either 'skeleton' or 'all'.
 * @return
 *   An array of skeleton tokens.
 */
function skeleton_token_list($type = 'all') {
  if ($type == 'skeleton' || $type == 'all') {
    $tokens = array();
    $tokens['skeleton'] = array();
    $result = db_query("SELECT * FROM {skeleton_token}");
    while ($token = db_fetch_object($result)) {
      $tokens['skeleton'][$token->token] = check_plain($token->description);
    }
    return $tokens;
  }
}

/**
 * Implementation of hook_token_values.
 *
 * @param $type
 *   The type of tokens to return.
 * @param $object
 *   An array of skeleton tokens with keys as tokens as values as the value to
 *   be replaced.
 * @param $options
 *   Any options passed to token_replace.
 * @return
 *   An array of tokens and their replacement values.
 */
function skeleton_token_values($type, $object = NULL, $options = array()) {
  $tokens = array();
  if ($type == 'skeleton') {
    $skeleton_tokens = $object;
    foreach ($skeleton_tokens as $token => $value) {
      $tokens[$token] = $value;
    }
  }
  return $tokens;
}

/**
 * Private function to generate HTML for showing the available tokens.
 *
 * @param type
 *   The token type to return, or 'all' for all tokens.
 * @return
 *   The themed representation of the available tokens.
 */
function _skeleton_build_token_help($type = 'all') {
  if ($type == 'skeleton') {
    return _skeleton_build_skeleton_token_help();
  }
  else {
    return _skeleton_build_other_token_help($type);
  }
}

/**
 * Build the help for custom skeleton tokens.
 * 
 * @return
 *   The themed representation of available skeleton tokens.
 */
function _skeleton_build_skeleton_token_help() {
  static $help_html;
  if (empty($help_html)) {
    $skeleton_tokens = array();
    $result = db_query("SELECT * FROM {skeleton_token}");
    while ($token = db_fetch_object($result)) {
      $token->description = check_plain($token->description);
      $skeleton_tokens[] = $token;
    }
    if (empty($skeleton_tokens)) {
      $help_html = '<p>' . t('No custom tokens have been created. <a href="@add-url">Add a token</a> to use them in skeleton outlines.', array('@add-url' => url('admin/content/skeleton/token/add')));
    }
    else {
      $help_html = theme('skeleton_token_help', $skeleton_tokens);
    }
  }
  return $help_html;
}

/**
 * Private function to generate HTML for showing the available tokens
 *
 * @param type
 *   The token type to return, or 'all' for all tokens.
 * @return
 *   The themed representation of the available tokens.
 */
function _skeleton_build_other_token_help($type = 'all') {
  if (!isset($help_html)) {
    static $help_html = array();
  }
  $help_html[$type] = '';
  if (empty($help_html[$type])) {
    if ($type == 'all') {
      $patterns = token_get_list();
    }
    else {
      $patterns = token_get_list($type);
      // We unset global tokens so they only show when 'all' is passed.
      unset($patterns['global']);
    }
    foreach ($patterns as $pattern_type => $pattern_set) {
      foreach ($pattern_set as $pattern => $description) {
        $tokens[$pattern] = $description;
      }
    }
    $help_html[$type] = theme('skeleton_other_token_help', $tokens);
  }
  return $help_html[$type];
}

/**
 * Replace tokens within the title, body, and an CCK text fields on a node.
 *
 * @param $node
 *   The node object to act upon.
 *
 * @param $type
 *   The token type to replace, corresponding to the types used with
 *   token_replace().
 *
 * @param $tokens
 *   The tokens to pass to token_replace().
 *
 * @param $fix_bookpathalias [optional]
 *   If the node contains [bookpathalias] tokens, replace them manually so that
 *   it can be used when the node hasn't been saved yet.
 *
 * @return
 *   The node object with the tokenized title, body, and CCK text fields.
 */
function _skeleton_token_replace_values($node, $type, $tokens, $fix_bookpathalias = FALSE) {
  // For subpages, we can simply simulate the [bookpathalias] token here.
  // Otherwise, it will be replaced with '' during token_replace() as the
  // node isn't saved yet.
  if ($fix_bookpathalias && module_exists('pathauto')) {
    $book_root = node_load($node->book['bid']);
    $node->title = str_replace('[bookpathalias]', drupal_get_path_alias($book_root->path), $node->title);
    $node->body = str_replace('[bookpathalias]', drupal_get_path_alias($book_root->path), $node->body);
  }
  $node->title = token_replace($node->title, $type, $tokens);
  $node->body = token_replace($node->body, $type, $tokens);
  // Also do any CCK text fields.
  $content_type = content_types($node->type);
  $fields = $content_type['fields'];
  foreach ($fields as $field) {
    if ($field['type'] == 'text' && is_array($node->{$field['field_name']})) {
      foreach (array_keys($node->{$field['field_name']}) as $delta) {
        $node->{$field['field_name']}[$delta]['value'] = token_replace($node->{$field['field_name']}[$delta]['value'], $type, $tokens);
      }
    }
  }
  return $node;
}

/**
 * theme_skeleton_token_help()
 *
 * @param $tokens
 *   An array of skeleton token objects.
 *
 * @return
 *   The themed representation of the tokens with the appropriate operations.
 */
function theme_skeleton_token_help($tokens) {
  $rows = array();
  foreach ($tokens as $token) {
    $row = array();
    $row[] = '[' . $token->token . ']';
    $row[] = $token->description;
    $row[] = theme('item_list', array(l(t('edit token'), 'admin/content/skeleton/token/' . $token->token_id . '/edit'), l(t('delete token'), 'admin/content/skeleton/token/' . $token->token_id . '/delete')));
    $rows[] = $row;
  }
  $header = array(t('Token'), t('Description'), t('Operations'));
  $tokens_html .= theme('table', $header, $rows);

  return $tokens_html;
}

/**
 * Theme help for tokens other than skeleton tokens.
 *
 * @param $tokens
 *   An array of tokens to display.
 * @return
 *   The themed representation of the tokens with the appropriate operations.
 */
function theme_skeleton_other_token_help($tokens) {
  $tokens_html = "";
  $rows = array();
  foreach ($tokens as $name => $description) {
    $row = array();
    $row[] = '['. $name .']';
    $row[] = $description;
    $rows[] = $row;
  }
  $header = array(t('Token'), t('Description'));
  $tokens_html .= theme('table', $header, $rows);

  return $tokens_html;
}
