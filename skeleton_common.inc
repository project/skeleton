<?php


/**
 * @file
 * Common functions for the skeleton module
 */

/**
 * Get the data for a skeleton from a title
 * @param $skeleton
 *   The name of the outline to act upon
 * @return $result
 *   An object containing elements from the {skeleton} table
 */
function skeleton_get_by_name($skeleton) {
  $result = db_fetch_object(db_query("SELECT * FROM {skeleton} WHERE skeleton = '%s'", $skeleton));
  return $result;
}

/**
 * Get the data for a skeleton template from a title
 *
 * @param $template
 *   The name of the template to act upon
 * @return $result
 *   An object containing elements from the {skeleton_template} table
 */
function skeleton_get_template_by_name($template) {
  $result = db_fetch_object(db_query("SELECT * FROM {skeleton_template} WHERE template = '%s'", $template));
  if ($result->node_data) {
    // node data is a complex serialized array
    $result->node_data = unserialize($result->node_data);
  }
  return $result;
}

/**
 * Get the tree structure for a skeleton outline
 * Function cirbbed from taxonomy_get_tree().
 *
 * @param $skeleton_id
 *   The id number of the skeleton outline being acted upon.
 * @param $parent
 *   The parent id of an item in the outline
 * @param $depth
 *   The depth of iterations through the parent-child relationship
 * @param $max_depth
 *   The maximum depth to traverse.
 * @return $tree
 *   A weighted array of templates assigned to this outline.
 */
function skeleton_get_tree($skeleton_id, $parent = 0, $depth = -1, $max_depth = NULL) {
  static $children, $parents, $templates, $storage;
  $depth++;
  // We cache trees, so it's not CPU-intensive to call get_tree() on a template
  // and its children, too.
  if (!isset($children[$skeleton_id][$parent])) {
    $children[$skeleton_id][$parent] = array();
    $result = db_query("SELECT sd.template_id, st.template, st.node_type, st.node_data, sd.parent, sd.weight FROM {skeleton_data} sd INNER JOIN {skeleton_template} st 
    ON sd.template_id = st.template_id WHERE sd.skeleton_id = %d ORDER BY sd.parent, sd.weight, st.template, sd.template_id DESC", $skeleton_id, $parent);
    while ($template = db_fetch_object($result)) {
      $children[$skeleton_id][$template->parent][] = $template->template_id;
      $parents[$skeleton_id][$template->template_id][] = $template->parent;
      // because we don't have a hierachy table like taxonomy, we run this query and store
      // children in a separate array to prevent duplication of terms
      $parental = 0;
      if ($template->parent) {
        $parental = db_result(db_query("SELECT parent FROM {skeleton_data} WHERE skeleton_id = %d AND template_id = %d", $skeleton_id, $template->parent));
      }
      if ($parental) {
        $storage[$skeleton_id][$parental][] = $template->template_id;
        $parents[$skeleton_id][$template->template_id][] = $parental;
      }
      $storage[$skeleton_id][$template->parent][] = $template->template_id;
      $templates[$skeleton_id][$template->template_id] = $template;
    }
  }
  // this section needs work, without the {taxonomy_hierarchy} table, the logic
  // for taxonomy_get_tree() isn't fully supported.  This seems to work.
  if ($parent && !empty($storage[$skeleton_id][$parent])) {
    foreach ($storage[$skeleton_id][$parent] as $key => $temp) {
      if (!empty($storage[$skeleton_id][$temp])) {
        foreach ($storage[$skeleton_id][$temp] as $t) {
          if (!in_array($t, $storage[$skeleton_id][$parent])) {
            $storage[$skeleton_id][$parent][] = $t;
          }
        }
      }
    }
  }
  $max_depth = (is_null($max_depth)) ? count($children[$skeleton_id]) : $max_depth;
  if ($children[$skeleton_id][$parent]) {
    foreach ($children[$skeleton_id][$parent] as $child) {
      if ($max_depth > $depth) {
        $template = drupal_clone($templates[$skeleton_id][$child]);
        $template->depth = $depth;
        $template->parents = $parents[$skeleton_id][$child];
        $tree[] = $template;
        // iterate again, if needed
        if ($children[$skeleton_id][$child]) {
          $tree = array_merge($tree, skeleton_get_tree($skeleton_id, $child, $depth, $max_depth));
        }
      }
    }
  }
  // kluge, see note at line 110.
  if (!empty($tree)) {
    foreach ($tree as $i => $element) {
      $tree[$i]->children = $storage[$skeleton_id][$element->template_id];
    }
  }
  return $tree ? $tree : array();
}
