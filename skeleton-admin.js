$(document).ready(function() {
  // If JS is enabled, then override the submit handler so that enter presses
  // on textfields don't trigger the no-JS fallback.
  $("form#skeleton-create-instance-form #edit-add-introduction").click(function(){
    // If any autocomplete fields are open, don't submit so the enter key can
    // be used to select an option.
    if ($("#autocomplete").length == 0) {
      $("form#skeleton-create-instance-form #edit-submit").click();
    }
    return false;
  });
});

/**
 * Add a checkbox to lists of nodes on the Syncronize page to select or clear
 * all checkboxes at once.
 */
Drupal.behaviors.skeletonAutoCheckbox = function(context) {
  var strings = { 'selectAll': Drupal.t('Select all rows in this table'), 'selectNone': Drupal.t('Deselect all rows in this table') };
  $('#skeleton-sync-content-form .form-item .form-checkboxes:not(.skeletonAutoCheckbox-processed)').prepend(
    $('<label></label>').html(Drupal.t('Select all nodes')).prepend($('<input type="checkbox" class="form-checkbox"/>').attr('title', strings.selectAll).click(function() {
      if (this.checked) {
        $('#skeleton-sync-content-form .skeletonAutoCheckbox-processed input[type=checkbox]').attr('checked', true);
      }
      else {
        $('#skeleton-sync-content-form .skeletonAutoCheckbox-processed input[type=checkbox]').attr('checked', false);
      }
    }))
  );

  $('#skeleton-sync-content-form .form-item .form-checkboxes').addClass('skeletonAutoCheckbox-processed');

  $('#skeleton-sync-add-template-form .form-item .form-checkboxes:not(.skeletonAutoCheckbox-processed)').prepend(
    $('<label></label>').html(Drupal.t('Select all nodes')).prepend($('<input type="checkbox" class="form-checkbox"/>').attr('title', strings.selectAll).click(function() {
      if (this.checked) {
        $('#skeleton-sync-add-template-form .skeletonAutoCheckbox-processed input[type=checkbox]').attr('checked', true);
      }
      else {
        $('#skeleton-sync-add-template-form .skeletonAutoCheckbox-processed input[type=checkbox]').attr('checked', false);
      }
    }))
  );
  $('#skeleton-sync-add-template-form .form-item .form-checkboxes').addClass('skeletonAutoCheckbox-processed');

  $('#skeleton-sync-delete-template-form .form-item .form-checkboxes:not(.skeletonAutoCheckbox-processed)').prepend(
    $('<label></label>').html(Drupal.t('Select all nodes')).prepend($('<input type="checkbox" class="form-checkbox"/>').attr('title', strings.selectAll).click(function() {
      if (this.checked) {
        $('#skeleton-sync-delete-template-form .skeletonAutoCheckbox-processed input[type=checkbox]').attr('checked', true);
      }
      else {
        $('#skeleton-sync-delete-template-form .skeletonAutoCheckbox-processed input[type=checkbox]').attr('checked', false);
      }
    }))
  );
  $('#skeleton-sync-delete-template-form .form-item .form-checkboxes').addClass('skeletonAutoCheckbox-processed');
}